package ee.evamaria.array;

import java.util.Scanner;

public class BubbleSortExample {

    public static void main(String[] args) {

        // ClassName anyVariableName = new ClassName();
        BubbleSortExample example = new BubbleSortExample();

        int[] arr = example.readInputFromUser();

        // Bubble sort started
        // 23,2,45,56,17
        int[] sortedArray = example.makeBubbleSort(arr);

        // Bubble sort ends

        example.printSortedArray(sortedArray);
    }

    public int[] readInputFromUser(){
        Scanner scan = new Scanner(System.in);

        System.out.println("Please, give some a set of natural numbers:");

        // 23,2,45,56,17
        // 2, 23, 45, 56, 17
        String line = scan.nextLine();
        String[] stringArray = line.split(",");

        int[] arr = new int[stringArray.length];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(stringArray[i]);
        }

        return arr;
    }

    public int[] makeBubbleSort(int[] unsortedArray){
        for (int i = 0; i < unsortedArray.length; i++) {
            for (int j = 0; j < unsortedArray.length-1-i; j++) {
                // 23 and 2
                if(unsortedArray[j]>unsortedArray[j+1])
                {
                    int temp=unsortedArray[j];
                    unsortedArray[j]=unsortedArray[j+1];
                    unsortedArray[j+1]=temp;

                    // temp = 23;
                    // 23 = 2;
                    // 2 = 23;
                }
            }
        }
        return unsortedArray;
    }

    public void printSortedArray(int[] sortedArray){
        System.out.println("Your numbers are sorted now:");

        for (int i = 0; i < sortedArray.length; i++) {
            System.out.print(sortedArray[i]+ " ");
        }
    }
}
