package ee.evamaria.array;

public class ArrayExample {

    public static void main(String[] args) {
/*        String apple = "apple";

        // 1 2 3 4 5
        // A p p l e
        // 0 1 2 3 4

        //'A', 'p', 'p', 'l', 'e'

        char chA = 'A';
        char chp1 = 'p';
        char chp2 = 'p';
        char chl = 'l';
        char che = 'e';

        // Array is data structure
        // Char is data type
        char[] arrayOfChars = new char[5];

        arrayOfChars[0] = 'A';
        arrayOfChars[1] = 'p';
        arrayOfChars[2] = 'p';
        arrayOfChars[3] = 'l';
        arrayOfChars[4] = 'e';

        int[] arrayOfIntegers = new int[5];

        // Adding
        arrayOfIntegers[0] = 24;
        arrayOfIntegers[1] = 56;
        arrayOfIntegers[2] = 17;
        arrayOfIntegers[3] = 46;

        // Update
        arrayOfIntegers[1] = 60;

        // Remove
        arrayOfIntegers[1] = 0;

        // Initializing
        char[] anyText = {'A', 'p', 'p', 'l', 'e'};

        // Array of strings
        String veryLong = "It is very long string";
        String it = "It";
        String is = "is";
        String very = "very";*/
        // ...

        String[] arrayOfStrings = new String[5];

        arrayOfStrings[0] = "Anything";
        // ...

        // Different ways of creating empty string
        String emptyString = "";
        String nullString = null;

        arrayOfStrings[1] = "It2";
        arrayOfStrings[2] = "It";
        arrayOfStrings[3] = "It";
        arrayOfStrings[4] = "Last element";

        System.out.println(arrayOfStrings[4]);

        // Array length
        System.out.println(arrayOfStrings.length);

        // Get last element from array
        System.out.println(arrayOfStrings[arrayOfStrings.length - 1]);
    }
}

