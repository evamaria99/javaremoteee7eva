package ee.evamaria.array;

public class FindDuplicatedExample {

    // write a java program to find duplicated values of any array
    // array might be integers
    // 1, 2, 5, 5, 6, 6, 7, 2

    public static void main(String[] args) {
        int[] arr = {1, 2, 5, 5, 6, 6, 7, 2};

        for (int i = 0; i < arr.length; i++) {
            // case i = 0
            // arr[0] = 1
            for (int j = i + 1;  j < arr.length; j++) {
                // j = 1;
                // arr[1] = 2
                if (arr[i] == arr[j] && (i != j)){

                    System.out.println( "Duplicate element: " +arr[j]);
                }
            }
        }
    }
}
