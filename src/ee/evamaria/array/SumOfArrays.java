package ee.evamaria.array;

public class SumOfArrays {

    // 1, 2, 5, 4, 7, 1, 3, 4
    // '3', 'a'
    // Find a sum of all values in this list

    // The result is 27

    public static void main(String[] args) {

        int[] myArray = {1, 2, 5, 4, 7, 1, 3, 4};

        int sum = 0;

        for (int i = 0; i < myArray.length; i++) {
            // ...
            sum = sum + myArray[i];

            // sum = 0;
            // sum = 1;
            // sum = 2;
            // sum = 3;
        }

        System.out.println("The sum is: "+sum);
        // After loop
    }
}
