package ee.evamaria.loop;

public class LoopExample {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            if (i == 3) {
                continue;
            }

            System.out.println(i);
        }

        // outer loop
        for (int i = 1; i <= 3; i++) {
            System.out.println("#");
            // second inner loop
            for (int j = 1; j <= 3; j++) {

                System.out.println("*");

                // third inner loop
                for (int k = 0; k < 3; k++) {
                    System.out.println("@");
                }
            }
        }

        for (int i = 1; i <= 3; i++) {

            //System.out.println("#");

            for (int j = 1; j <= 3; j++) {

                if (i == 2 && j == 2) {
                    // no instruction
                    continue;
                }

                System.out.println("*");
            }
        }

        for (int i = 0; i < 20; i = i + 2) {
            // 0, 2, 4, 6, 8 ...

            if (i == 4) {
                continue;
            }

            System.out.println(i);
        }
    }
}
