package ee.evamaria.methods.overloading;

import java.text.Normalizer;

public class MethodOverloadingExample {
    public static void main(String[] args) {

// method overloading with the example when number of arguments are changed
        Adder adder = new Adder();
        adder.add(1, 4);
        adder.add(5,7, 9);
                adder.add(35, 5 , 7, 9);


                        // when method argument type is changed
        FormatService formatService = new FormatService();

        String years = formatService.formatNumber(6);
        String money = formatService.formatNumber(50.60);

        System.out.println(years);
        System.out.println(money);
    }
}
