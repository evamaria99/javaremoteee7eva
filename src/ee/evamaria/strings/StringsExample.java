package ee.evamaria.strings;

    import java.util.Scanner;

    class StringExample {

        public static void main(String[] args) {

            Scanner scan = new Scanner(System.in);

            String textLine = scan.nextLine();

            System.out.println("You entered this text in the console: " +textLine);

            int number = scan.nextInt();

            System.out.print("You entered a number: " +number);

            // Char at example
            String text = "java remote ee 7";

            char ch = text.charAt(5);

            System.out.println(ch);

            // Concat
            String iAm = "I am a student at ";

            String iAmAndText = iAm.concat(text);

            System.out.println(iAmAndText);

            // Contains
            String text2 = "java";

            boolean exist = text.contains(text2);

            System.out.println(exist);

            // Start with

            boolean yes = text.endsWith("java");
        }
    }

