package ee.evamaria.objectandclasses;

public class Investment {

    /**
     * Write a Java method to compute the future investment
     * value at a given interest rate for a specified number of years.
     *
     * Sample data (Monthly compounded):
     * Input the investment amount: 1000
     * Input the rate of interest: 10
     * Input number of years: 5
     */

    public double futureInvestmentValue(double investmentAmount,
                                        double monthlyInterestRate,
                                        int years){
        // future value = present value x [1 + (interest rate x time)]
        return investmentAmount * Math.pow(1 + monthlyInterestRate, years * 12);
    }

    /**
     * Input the investment amount: 1000
     * Input the rate of interest: 10
     * Input number of years: 5
     *
     * Years    FutureValue
     * 1            1104.71
     * 2            1220.39
     * 3            1348.18
     * 4            1489.35
     * 5            1645.31
     */
    public void displayResultInvestmentValue(int year,
                                             double investmentAmount,
                                             double yearInterestRate){

        System.out.println("Years   FutureValue");

        for (int i = 1; i < year; i++) {
            int formatter = 19;
            if(i>=10) {
                formatter = 18;
            }

            // %{some number}.2f, this is formula for formatting
            // .2f means formatting it as a decimal

            System.out.printf(i + "%" + formatter+ ".2f\n",
                    futureInvestmentValue(investmentAmount, yearInterestRate/12, i));

            /*System.out.println(i + "   " +
                    futureInvestmentValue(investmentAmount, yearInterestRate/12, i));*/
        }
    }
}
