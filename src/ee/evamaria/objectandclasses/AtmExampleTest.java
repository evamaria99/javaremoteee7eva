package ee.evamaria.objectandclasses;

    public class AtmExampleTest {

        public static void main(String[] args) {

            //class is a custom data type created by a developer


            AtmExample atmExample = new AtmExample("777");
            atmExample.insert(20000);

            // One user comes and tries to deposit some money
            atmExample.deposit(300, 6);
            // After sometime, user comes again and withdraws some money
            atmExample.withdraw(100, 6);
            // user wants to check balance
            atmExample.checkBalance(6);

            // Check ATM balance
            atmExample.displayAtmBalance();
        }
    }
