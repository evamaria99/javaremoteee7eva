package ee.evamaria.objectandclasses;

    public class DifferentClass {

        public static void main(String[] args) {

            Student student1 = new Student("12345");

            student1.firstName = "John";
            student1.lastName = "Smith";
            student1.faculty = "Computer science";

            //..

            student1.getId();
        }
    }
