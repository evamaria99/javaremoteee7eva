package ee.evamaria.objectandclasses.addressbook;

/* public class Person {
    //mandatory fields
    private String firstName;
    private String lastName;
    private int age;
    //Optional fields
    //composition- when one class is inside another class and it is as a field
    private Address address;


    //constructor (generate-> constructor -> choose firstName, lastname and age)
    public Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    // getter and setter
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}

 */
