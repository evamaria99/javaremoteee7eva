package ee.evamaria.datastructures;

/**
 *
 * Queue
 * 12, 56, 76, 89, 100, 343, 21, 234
 *
 * add: 5
 *
 * 5, 12, 56, 76, 89, 100, 343, 21, 234
 *
 * remove: 21
 *
 * 5, 12, 56, 76, 89, 100, 343
 */


public class QueueExample {
    /**also possible way of writing:
    int front;
    int end;
    int size;
    **/

    int front, end, size;
    int max;
    int array[];

    public QueueExample(int max) {
        this.max = max;
        this.array = new int[max];
        front = 0;
        end = max - 1;
        size = 0;
    }

    public void enqueue(int newElement){
        if (size == max){
            System.out.println("Sorry the queue is full");
            return;
        }
        // % find a reminder from division
        end = (end + 1) % max;

        array[end] = newElement;
        size++;

        System.out.println("New element is added");

    }

    public int dequeue (){

        if(size == 0){
            System.out.println("the queue is empty");
        }

        int removedItem = array[front];

        front = (front + 1) % max;
        size--;

        return removedItem;
    }

    public static void main(String[] args) {
        QueueExample newQueue = new QueueExample(20);


        newQueue.enqueue(7);
        newQueue.enqueue(5);
        newQueue.enqueue(41);

        System.out.println("Take an element from the queue" + newQueue.enqueue(););
    }
}
