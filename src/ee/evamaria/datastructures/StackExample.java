package ee.evamaria.datastructures;

/**
 * Array:
 * 12, 56, 76, 89, 100, 343, 21, 234 -> 77, 88, 25 array adds to the end new elements
 *
 * stack:
 *
 * add(77, 88, 25)
 * 77, 88, 25, 12, 56, 76, 89, 100, 343, 21, 234 stack adds the elements to the beginning
 *
 * remove (88)
 *
 * 25, 77, 12, 56, 76, 89, 100, 343, 21, 234 first deletes and then adds
 *
 * first in last out
 */

// TODO Homework: try to think any real world example about usage of stack (first in, last out)

public class StackExample {

    private int[] array;
    private int numberOfElements;
    private int max;

    public StackExample(int max) {
        this.array = new int[max];
        this.numberOfElements = 0;
        this.max = max;
    }

    public void push(int newElement) {

        if (numberOfElements >= (max)) {
            System.out.println("Sorry, stack is full");
        } else {
            array[numberOfElements] = newElement;
            numberOfElements++;

        }

    }

    public int pop() {

        if (numberOfElements == 0) {
            System.out.println("Sorry, stack is empty, we cannot remove");

        } else {
            numberOfElements--;
            int removedElement = array[numberOfElements];

        }
    }

// push -> adding element to the stack
        //pop -> removing /getting/taking element from the stack

        public static void main (String[]args){

            StackExample newStack = new StackExample(40);

            newStack.push(234);
            newStack.push(21);
            newStack.push(343);
            newStack.push(100);
            newStack.push(89);
            newStack.push(76);
            newStack.push(56);
            newStack.push(12);

            System.out.println("The element taken from the top:" + newStack.pop());
            System.out.println("take another element:" + newStack());
            System.out.println("take another element:" + newStack());

        }
    }

