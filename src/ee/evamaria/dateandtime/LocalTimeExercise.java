package ee.evamaria.dateandtime;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

public class LocalTimeExercise {

    public static void main(String[] args) {
        // write a small program to get the months remaining in the year

        LocalDate today = LocalDate.now();
        LocalDate lastDayOfTheYear = today.with(TemporalAdjusters.lastDayOfYear());

        Period period = today.until(lastDayOfTheYear);

        System.out.println("Months remaining in the year 2021: " + period.getMonths());

        LocalDate anotherToday = LocalDate.now();
        System.out.println("Format it like dd/mm/yyyy  "
        + anotherToday.format(DateTimeFormatter.ofPattern("dd/MM/yy")));
    }
}
//result:months remaining in the year 2021: 8