package ee.evamaria.dateandtime;

import java.time.LocalDate;
import java.time.LocalTime;

public class LocalTimeExample {

    public static void main(String[] args) {
        LocalTime timeNow = LocalTime.now();

        System.out.println( "Now time is " + timeNow);
        System.out.println("Our lesson will finish today at " + timeNow
        .plusHours(1)
        .plusMinutes(10));

        LocalDate  localDate = LocalDate.now();
        System.out.println("Today's date is : " + localDate);
    }
}
