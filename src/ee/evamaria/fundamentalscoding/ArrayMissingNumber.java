package ee.evamaria.fundamentalscoding;


/** WORKING.
 * find missing number in list of elements
 *
 * Note: numbers are countable
 *
 * 7, 5, 6, 1, 4, 2  -> 3
 *
 *
 */

import java.util.Arrays;

/**
 * 1. find sum of n numbers 1, 2, 3... n = 3, answer = 6
 *
 * n=n*(n+1)/2 -> formula
 *
 *      1 + 2 + 3 + 4 + 5 + 6 + 7 = 28
 *
 * 2. find the sum of elements in the array
 *
 *      7 + 5 + 6 + 1 + 4 + 2 = 25
 *
 * 3. (sum of n numbers) - (sum of elements in the array) = missing number
 *
 *      28 - 25 = 3
 */

public class ArrayMissingNumber {

    public static void main(String[] args) {

        int[] input = { 7, 5, 6, 1, 4, 2 };

        System.out.println("For the given array :" + Arrays.toString(input));
        System.out.println("Missing number is " + missingNumber(input));

    }

    public static int missingNumber(int[] input){

        int n = input.length + 1;
        // plus one because one number is missing?

        System.out.println("Array lenght is: " +n);
        int sumofNumbers = n*(n+1)/2;

        int sumOfArrayElements = 0;
        for (int i = 0; i < input.length; i++) {
            sumOfArrayElements = sumOfArrayElements + input[i];

        }
        return sumofNumbers - sumOfArrayElements;
    }

}

