package ee.evamaria.fundamentalscoding;

import java.util.Scanner;

/** WORKING!
 * Check if given string has all unique chars
 *
 * Apple - has not all unique chars
 * world - has all unique chars
 */

public class StringUniqueExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Write a word: ");

        String firstString = scanner.next();


        System.out.println("Given strings us unique " + isUnique(firstString));

    }

    public static boolean isUnique(String word){

        for (int i = 0; i < word.length(); i++) {

            char letter = word.charAt(i);

            if (word.indexOf(letter) != word.lastIndexOf(letter)){
                return false;

            }

        }
        return true;
    }
}


