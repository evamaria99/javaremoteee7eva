package ee.evamaria.fundamentalscoding;

import com.sun.xml.internal.ws.api.ha.StickyFeature;

import java.util.Scanner;

/** WORKING.
 * Anagrams mean if two Strings have the same characters but in a different order.
 * For example: Angel and Angle are anagrams
 */


public class StringAnagramExample {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Write the first string:");

        String firstString = scanner.next();

        System.out.println("Write the second string");

        String secondString = scanner.next();

        System.out.println("Given strings are anagrams: " + IsAnagram(firstString, secondString));
    }

    public static boolean IsAnagram(String firstString, String secondString){

        if(firstString.length() != secondString.length()) {
            return false;
        }

        for (int i = 0; i < firstString.length(); i++) {

            char letter = firstString.charAt(i);

            int index = secondString.indexOf(letter);

            if(index == -1) {

                return  false;
            }

            }

        return true;
        }

    }

