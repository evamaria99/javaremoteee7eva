package ee.evamaria.HOMEWORK;

public class homework1 {

    //TODO: Homework 1:
    // Find any real world Object which has some parts and try to build it with
    // Inner classes as we did with PC.

    public static void main(String[] args) {

        DishesSet dishes = new DishesSet();

    dishes.setPrice(79.99);
    dishes.manufacturer = "Ikea";

    DishesSet.dessert dessert = dishes.new dessert ();
        dessert.pieces1 = 6;
        dessert.diameter1 = 13.5;

        DishesSet.soupBowl soupBowl = dishes.new soupBowl();
        soupBowl.pieces2 = 4;
        soupBowl.diameter2 = 10.2;

        DishesSet.dinnerPlate dinnerPlate = dishes.new dinnerPlate();
        dinnerPlate.pieces3 = 12;
        dinnerPlate.diameter3 = 13.7;

        DishesSet.cup cup = dishes.new cup();
        cup.pieces4 = 8;
        cup.capacity = 250;

        int sum = dessert.pieces1 + soupBowl.pieces2 + dinnerPlate.pieces3 + cup.pieces4;
        double result = dishes.price/sum;

        System.out.println("The set of " + dishes.manufacturer + " dishes consist of " +sum + " pieces");
        System.out.println("That makes the price per piece " +result + " EUR" );

    }
}