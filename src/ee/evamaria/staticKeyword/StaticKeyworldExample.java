package ee.evamaria.staticKeyword;

public class StaticKeyworldExample {

    public static void main(String[] args) {

        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();
        Student student4 = new Student();

        student1.setAge(20);
        student1.setFirstName("John");
        student1.setLastName("Smith");


        student2.setAge(23);
        student2.setFirstName("Anna");
        student2.setLastName("Manna");

        System.out.println(Student.faculty);

        Student.study();
    }
}
