package ee.evamaria.innerclasses;

public class InnerClassExample {


    public static void main(String[] args) {

        PersonalComputer pc = new PersonalComputer();

        pc.setPrice(1200);
        PersonalComputer.CPU cpu = pc.new CPU();
        cpu.cores = 7;
        cpu.manufacturer = "Intel Core";

        PersonalComputer.RAM ram = pc.new RAM();
        ram.clockspeed = 2.2;
        ram.memory = 16;

        PersonalComputer.SSD ssd = pc.new SSD();

        System.out.println("Ram Clock speed = " + ram.clockspeed);

    }
}
