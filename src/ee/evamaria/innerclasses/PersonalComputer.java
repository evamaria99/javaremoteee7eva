package ee.evamaria.innerclasses;

public class PersonalComputer {

    double price;

    class CPU {
        double cores;
        String manufacturer;

    }
        class RAM{
        int memory;
        double clockspeed;
    }
        class SSD{
        double memorysize;
        }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
